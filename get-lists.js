'use strict';

const puppeteer = require('puppeteer');

const minimist = require('minimist');

let args = minimist(process.argv.slice(2));
async function getList(){
  const browser = await puppeteer.launch({headless : false})
  const page = await browser.newPage()
 //  await page.tracing.start({
 //    path: 'trace.json',
 //    categories: ['devtools.timeline']
	// })
  await page.goto('https://news.ycombinator.com/news')

  // execute standard javascript in the context of the page.
  const stories = await page.$$eval('a.storylink', anchors => { return anchors.map(anchor => anchor.textContent).slice(0, 10) })
  console.log(stories)
  // await page.tracing.stop();
  await browser.close()
}
async function screenshot(){
  const browser = await puppeteer.launch({headless : false})
  const page = await browser.newPage()
  await page.setViewport({ width: 1280, height: 800 })
  await page.goto('https://www.nytimes.com/')
  await page.screenshot({ path: 'nytimes.png', fullPage: true })
  await browser.close()
}


async function search(){
	 const browser = await puppeteer.launch({headless : false})
    const page = await browser.newPage()
    await page.goto('https://youtube.com')
    await page.type('#search', 'em gì ơi')
    await page.click('button#search-icon-legacy')
    await page.waitForSelector('ytd-thumbnail.ytd-video-renderer')
    await page.screenshot({path: 'youtube_fm_dreams_list.png'})
    const videos = await page.$$('ytd-thumbnail.ytd-video-renderer')
    await videos[2].click()
    await page.waitForSelector('.html5-video-container')
    await page.waitFor(5000)
    await page.screenshot({ path: 'youtube.png' })
    await browser.close()
}

async function github(){
 	const browser = await puppeteer.launch({headless: false})
  const page = await browser.newPage()
  await page.goto('https://github.com/login')
  await page.type('#login_field', "nam94-lego")
  await page.type('#password', "Hoainam23894")
  await page.click('[name="commit"]')
  await page.waitForNavigation()
  await page.screenshot({ path: 'github.png' })
  browser.close()
}	
// async function setCokkies(){
// 	  const browser = await puppeteer.launch({headless : false})
// 	  const page = await browser.newPage()
// 	  await page.setCookie(cookie)
// 	  await page.goto('https://erp.nhanh.vn/user/signin')
// 	  await page.screenshot({ path: 'paypal_login.png' })
// 	  await browser.close()
// }

if(args){
	switch(args.type){
		case 'getList' :
			getList();
			break;
		case 'screenshot' : 
			screenshot();
			break;
		case 'setCokkies' : 
			setCokkies();
			break;
		case 'search' : 
		search();
		break;
		case 'github' : 
		github();
		break;
	}
}
