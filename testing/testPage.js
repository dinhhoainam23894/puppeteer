const { extractDataFromPerformanceTiming , getTimeFromPerformanceMetrics,
  extractDataFromPerformanceMetrics } = require('./helpers');

async function testPage(page) {
   const client = await page.target().createCDPSession();
   await client.send('Performance.enable');
  await page.goto('https://cunghocvui.com');

  await page.waitFor(1000);
  const performanceMetrics = await client.send('Performance.getMetrics');

  // const performanceTiming = JSON.parse(
  //   await page.evaluate(() => JSON.stringify(window.performance.timing))
  // );

  // return extractDataFromPerformanceTiming(
  //   performanceTiming,
  //   'responseEnd',
  //   'domInteractive',
  //   'domContentLoadedEventEnd',
  //   'loadEventEnd'
  // );

  return extractDataFromPerformanceMetrics(
    performanceMetrics,
    'FirstMeaningfulPaint',
  );
}

module.exports = testPage;