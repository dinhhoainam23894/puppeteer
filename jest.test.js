let puppeteer = require('puppeteer');
let browser = null;
let page = null;


describe('Lazada test', () => {

    // Code này được chạy khi bắt đầu chạy unit test
    beforeAll(async() => {
        browser = await puppeteer.launch({headless : false});
        page = await browser.newPage();
        await page.setViewport({
            width: 1280,
            height: 720
        });
      
        // Mặc định, timeout của jest là 5s. 
        // Vì web load có thể lâu nên ta tăng lên thành 60s.
        jest.setTimeout(60000);
    });

    // Đóng trình duyệt sau khi đã chạy xong các test case
    afterAll(async() => {
        await browser.close();
    });

    // Trước khi chạy mỗi test case, vào trang chủ của lazada
    beforeEach(async() => {
        await page.goto('https://lazada.vn');
    });

    test('Search iphone', async() => {
    expect.assertions(1);
    // Tìm khung search, gõ sexy underwear và bấm enter
    const searchBox = await page.$('#q');
    await searchBox.type('iphone 6s');
    await searchBox.press('Enter');

    // Chờ trang load xong, tìm các phần tử item và đếm nếu đủ 40
    await page.waitForNavigation();
     await page.screenshot({ path: 'lazada.png', fullPage: true })
    const products = await page.$$('div[data-qa-locator=product-item]');
    expect(products.length).toBe(40);
	});

	// test('Install app', async() => {
	//     expect.assertions(1);
	//     // Tìm và click vào link
	//     const appDownloadLink = await page.$x('//*[@id="topActionDownload"]');
	//     await appDownloadLink[0].click();

	//     await page.waitForNavigation();
	//     // Tìm element trên menu, lấy innerText của element đó  
	//     const breadCrumbHandle = await page.$x('/html/body/header/footer/div[2]/div[1]/ul/li[2]/span');
	//     const text = await page.evaluate(element => element.innerText, breadCrumbHandle[0]);

	//     // Check nội dung element đó có chữ App Mobile tại Lazada
	//     expect(text).toContain('App Mobile tại Lazada');
	// });

})

